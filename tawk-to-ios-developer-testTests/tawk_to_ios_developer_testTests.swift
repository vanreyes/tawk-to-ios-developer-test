//
//  tawk_to_ios_developer_testTests.swift
//  tawk-to-ios-developer-testTests
//
//  Created by Vandolph Reyes on 5/27/21.
//

import XCTest
@testable import tawk_to_ios_developer_test

class tawk_to_ios_developer_testTests: XCTestCase {
    
    func testValidateNote() {
        
        let validation = ValidationService()
        
        XCTAssertNoThrow(try validation.validateNote("This is my notes"))
    }

}
