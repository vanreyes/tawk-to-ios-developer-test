//
//  CoreDataService.swift
//  tawk-to-ios-developer-test
//
//  Created by Vandolph Reyes on 5/31/21.
//

import UIKit

class CoreDataService {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var githubUserDetail:GithubUserDetail?
    
    func addGithubUser(login: String, id: Int64, avatar_url: String, site_admin: Bool, url: String) {
        let newGithubUser = GithubUserDetail(context: context)
        newGithubUser.login = login
        newGithubUser.id = id
        newGithubUser.avatar_url = avatar_url
        newGithubUser.site_admin = site_admin
        newGithubUser.url = url
        
        do {
            try context.save()
        } catch {
            print("error")
        }
    }
    
    func updateGithubUser(githubUser: GithubUserDetail, newNote: String) {
        githubUser.notes = newNote
        
        do {
            try context.save()
        } catch {
            print("error")
        }
    }
}
