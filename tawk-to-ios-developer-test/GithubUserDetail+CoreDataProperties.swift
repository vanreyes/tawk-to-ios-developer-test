//
//  GithubUserDetail+CoreDataProperties.swift
//  tawk-to-ios-developer-test
//
//  Created by Vandolph Reyes on 5/28/21.
//
//

import Foundation
import CoreData


extension GithubUserDetail {
    
    @nonobjc public class func fetchRequest(search: String) -> NSFetchRequest<GithubUserDetail> {
        
        let containsUsername = NSPredicate(format: "login CONTAINS[cd] %@", search)
        let containsNotes    = NSPredicate(format: "notes CONTAINS[cd] %@", search)
        let fetchRequest     = NSFetchRequest<GithubUserDetail>(entityName:"GithubUserDetail")
        
        if !search.isEmpty {
            fetchRequest.predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [containsUsername, containsNotes])
        }
        
        return fetchRequest;
        
    }
    
    @NSManaged public var id: Int64
    @NSManaged public var login: String?
    @NSManaged public var avatar_url: String?
    @NSManaged public var url: String?
    @NSManaged public var site_admin: Bool
    @NSManaged public var notes: String?
    
}

extension GithubUserDetail : Identifiable {
    
}
