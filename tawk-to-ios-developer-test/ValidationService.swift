//
//  ValidationService.swift
//  tawk-to-ios-developer-test
//
//  Created by Vandolph Reyes on 5/31/21.
//

import Foundation

struct ValidationService {
    func validateNote(_ notes: String?) throws -> String {
        
        guard let notes = notes else {
            throw ValidationError.emptyValue
        }
        
        guard notes.count > 3 else {
            throw ValidationError.notesTooShort
        }
        
        guard notes.count < 20 else {
            throw ValidationError.notesTooLong
        }
        
        return notes
        
    }
}

enum ValidationError: LocalizedError {
    case emptyValue
    case notesTooShort
    case notesTooLong
    
    var errorDescription: String? {
        
        switch self {
        case .emptyValue:
            return "Notes must not be empty"
        case .notesTooShort:
            return "Notes must not atleast 3 letters in length"
        case .notesTooLong:
            return "Notes too long"
        }
    }
}
