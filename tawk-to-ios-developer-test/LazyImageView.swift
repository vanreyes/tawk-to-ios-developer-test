//
//  LazyImageView.swift
//  tawk-to-ios-developer-test
//
//  Created by Vandolph Reyes on 5/29/21.
//  Source: https://www.youtube.com/watch?v=Bytv3PJjNi4

import Foundation
import UIKit

class LazyImageView: UIImageView
{
    private let imageCache = NSCache<AnyObject, UIImage>()
    
    func loadImage(fromURL imageURL: URL, placeHolderImage: String, inverted: Bool)
    {
        self.image = UIImage(named: placeHolderImage)
        
        if let cachedImage = self.imageCache.object(forKey: imageURL as AnyObject)
        {
            debugPrint("image loaded from cache for =\(imageURL)")
            
            self.image = cachedImage
            return
        }
        
        DispatchQueue.global().async {
            [weak self] in
            
            if let imageData = try? Data(contentsOf: imageURL)
            {
                //debugPrint("image downloaded from server...")
                if let image = UIImage(data: imageData)
                {
                    // updated
                    // Source:  https://amilarblog.wordpress.com/2016/02/02/how-to-invert-the-colors-of-an-image-in-swift/
                    var finalImage = UIImage()
                    if inverted == true {
                        
                        let origImage = CIImage(image: image)
                        let filter = CIFilter(name: "CIColorInvert")
                        filter!.setValue(origImage, forKey: kCIInputImageKey)
                        finalImage = UIImage(ciImage: (filter?.outputImage!)!)
                        
                    } else {
                        
                        finalImage = image
                        
                    }
                    
                    DispatchQueue.main.async {
                        self!.imageCache.setObject(finalImage, forKey: imageURL as AnyObject)
                        self?.image = finalImage
                    }
                }
            }
        }
    }
}
