//
//  ViewController.swift
//  tawk-to-ios-developer-test
//
//  Created by Vandolph Reyes on 5/27/21.
//

import UIKit

let reachability = try! Reachability()

class GithubUsersViewController: UIViewController, UITextFieldDelegate {
    
    // MARK:- Outlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    
    // MARK:- Variables
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var models          = [GithubUserDetail]()
    var coreDataService = CoreDataService()
    
    var githubUsers: [GithubUsers] = []
    var lastId:Int64 = 0
    var enablePagination:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set delegates
        searchTF.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        reachability.whenUnreachable = { _ in
            print("no internet")
            self.showAlert(message: "You are offline")
        }
        
        do {
            try
                reachability.startNotifier()
        } catch {
            print("unable to start notifier")
        }
        
        // set search icon
        searchTF.setLeftView(image: UIImage.init(named: "SearchIcon")!)
        searchTF.tintColor = .darkGray
        
        // on first load
        // check if data exist in CoreData
        // then show it
        // else
        // fetch from the API

        // check if coredata have data
        getAllGithubUsers(search: "")
        if (models.count == 0) {
            print("fetching from API")
            fetchGithubUserList(index: 0)
        } else {
            print("fetching from CoreData")
        }
        
        // get coredata file path
        let path = FileManager
            .default
            .urls(for: .applicationSupportDirectory, in: .userDomainMask)
            .last?
            .absoluteString
            .replacingOccurrences(of: "file://", with: "")
            .removingPercentEncoding
        print("\(String(describing: path))")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllGithubUsers(search: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // check internet is back
        reachability.whenReachable = {
            reachability in
            if reachability.connection == .wifi {
                print("Connected to Internet: WiFi")
                self.showAlert(message: "Successfully connected to Internet: WiFi")
            } else {
                print("Connected to Internet: Mobile Data")
                self.showAlert(message: "Successfully connected to Internet: Mobile Data")
            }
        }
            
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func searchingUser(_ sender: Any) {
        getAllGithubUsers(search: searchTF.text!)
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Internet Connection", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: {_ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // get users from github API
    private func fetchGithubUserList(index: Int64) {
        print("fetching users list since \(index)")
        
        // DispatchSemaphore to queued 1 request at a time
        let semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: "https://api.github.com/users?since=\(index)")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
            }
            //print(String(data: data, encoding: .utf8)!)
            
            // https://www.avanderlee.com/swift/json-parsing-decoding/
            self.githubUsers = try! JSONDecoder().decode([GithubUsers].self, from: data)
            print(self.githubUsers.count)
            
            for githubUser in self.githubUsers {
                self.coreDataService.addGithubUser(login: githubUser.login!, id: Int64(githubUser.id), avatar_url: githubUser.avatar_url!, site_admin: githubUser.site_admin, url: githubUser.url!)
            }
            
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
        
        getAllGithubUsers(search: "")
        tableView.reloadData()
    }
    
    func getAllGithubUsers(search: String) {
        
        // disable pagination when use is trying to search
        if search.isEmpty {
            enablePagination = true
        } else {
            enablePagination = false
        }
        
        do {
            models = try context.fetch(GithubUserDetail.fetchRequest(search: search))
            
            DispatchQueue.main.async { [self] in
                self.tableView.reloadData()
            }
        } catch {
            print("error")
        }
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
}

// generated from my helper
// https://jsfiddle.net/vandolphreyes29/4ewzb1hq/225/
struct GithubUsers: Codable {
    let login: String?
    let id: Int
    let node_id: String?
    let avatar_url: String?
    let gravatar_id: String?
    let url: String?
    let html_url: String?
    let followers_url: String?
    let following_url: String?
    let gists_url: String?
    let starred_url: String?
    let subscriptions_url: String?
    let organizations_url: String?
    let repos_url: String?
    let events_url: String?
    let received_events_url: String?
    let type: String?
    let site_admin: Bool
}

extension GithubUsersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vc = storyboard?.instantiateViewController(identifier: "GithubUserDetailsViewController") as? GithubUserDetailsViewController {
            
            vc.title = models[indexPath.row].login
            vc.githubUserUrl = models[indexPath.row].url!
            vc.githubUserDetail = models[indexPath.row]
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension GithubUsersViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // check if image should be inverted
        var inverted = false
        if (Int(indexPath.row) != 0 && Int(indexPath.row) % 3 == 0) {
            // inverted cell
            inverted = true
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "invertedCell") as! GithubUsersTableViewInvertedCell
            
            cell.githubUserUsernameLbl.text = models[indexPath.row].login
            cell.githubUserNodeLbl.text = "Site Admin: \(models[indexPath.row].site_admin)"
            
            if models[indexPath.row].notes == nil {
                cell.notesIconImg.isHidden = true
            } else {
                cell.notesIconImg.isHidden = false
            }
            
            // loading image directly from URL
            let imageUrl = URL(string: (models[indexPath.row].avatar_url!))
            //let imageUrlData = try? Data(contentsOf: imageUrl!)
            //cell.githubUserAvatarImg.image = UIImage(data: imageUrlData!)
            
            // lazy loading
            cell.githubUserAvatarImg.loadImage(fromURL: imageUrl!, placeHolderImage: "GithubUserIcon", inverted: inverted)
            cell.githubUserAvatarImg.layer.borderWidth = 1
            cell.githubUserAvatarImg.layer.masksToBounds = false
            cell.githubUserAvatarImg.layer.borderColor = UIColor.lightGray.cgColor
            cell.githubUserAvatarImg.layer.cornerRadius = cell.githubUserAvatarImg.frame.height/2
            cell.githubUserAvatarImg.clipsToBounds = true
            
            return cell
        } else {
            
            // normal cell
            if models[indexPath.row].notes == nil {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! GithubUsersTableViewNormalCell
                
                
                cell.githubUserUsernameLbl.text = models[indexPath.row].login
                cell.githubUserNodeLbl.text = "Site Admin: \(models[indexPath.row].site_admin)"
                
                if models[indexPath.row].notes == nil {
                    cell.notesIconImg.isHidden = true
                } else {
                    cell.notesIconImg.isHidden = false
                }
                
                // loading image directly from URL
                let imageUrl = URL(string: (models[indexPath.row].avatar_url!))
                //let imageUrlData = try? Data(contentsOf: imageUrl!)
                //cell.githubUserAvatarImg.image = UIImage(data: imageUrlData!)
                
                // lazy loading
                cell.githubUserAvatarImg.loadImage(fromURL: imageUrl!, placeHolderImage: "GithubUserIcon", inverted: inverted)
                cell.githubUserAvatarImg.layer.borderWidth = 1
                cell.githubUserAvatarImg.layer.masksToBounds = false
                cell.githubUserAvatarImg.layer.borderColor = UIColor.lightGray.cgColor
                cell.githubUserAvatarImg.layer.cornerRadius = cell.githubUserAvatarImg.frame.height/2
                cell.githubUserAvatarImg.clipsToBounds = true
                
                return cell
                
            } else {
                
                // notes cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "notesCell") as! GithubUsersTableViewNotesCell
                
                cell.githubUserUsernameLbl.text = models[indexPath.row].login
                cell.githubUserNodeLbl.text = "Site Admin: \(models[indexPath.row].site_admin)"
                
                if models[indexPath.row].notes == nil {
                    cell.notesIconImg.isHidden = true
                } else {
                    cell.notesIconImg.isHidden = false
                }
                
                // loading image directly from URL
                let imageUrl = URL(string: (models[indexPath.row].avatar_url!))
                //let imageUrlData = try? Data(contentsOf: imageUrl!)
                //cell.githubUserAvatarImg.image = UIImage(data: imageUrlData!)
                
                // lazy loading
                cell.githubUserAvatarImg.loadImage(fromURL: imageUrl!, placeHolderImage: "GithubUserIcon", inverted: inverted)
                cell.githubUserAvatarImg.layer.borderWidth = 1
                cell.githubUserAvatarImg.layer.masksToBounds = false
                cell.githubUserAvatarImg.layer.borderColor = UIColor.lightGray.cgColor
                cell.githubUserAvatarImg.layer.cornerRadius = cell.githubUserAvatarImg.frame.height/2
                cell.githubUserAvatarImg.clipsToBounds = true
                
                return cell
                
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == models.count - 1 && enablePagination == true {
            print("fetching more users starting from \(self.models[indexPath.row].id)")
            
            let spinner = UIActivityIndicatorView(style: .large)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            
            delayWithSeconds(3) {
                self.fetchGithubUserList(index: self.models[indexPath.row].id)
                
                self.lastId = self.models[indexPath.row].id
                
                tableView.tableFooterView?.isHidden = true
            }
        }
    }
}

extension UITextField {
    func setLeftView(image: UIImage) {
        let iconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 25, height: 25)) // set your Own size
        iconView.image = image
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 45))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        self.tintColor = .lightGray
    }
}
