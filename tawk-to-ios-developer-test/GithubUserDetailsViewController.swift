//
//  GithubUserDetailsViewController.swift
//  tawk-to-ios-developer-test
//
//  Created by Vandolph Reyes on 5/27/21.
//

import UIKit

class GithubUserDetailsViewController: UIViewController, UITextViewDelegate {
    
    // MARK:- Outlets
    @IBOutlet weak var githubUserAvatarImg: LazyImageView!
    @IBOutlet weak var githubUserFollowersLbl: UILabel!
    @IBOutlet weak var githubUserFollowingLbl: UILabel!
    @IBOutlet weak var githubUserNameLbl: UILabel!
    @IBOutlet weak var githubUserCompanyLbl: UILabel!
    @IBOutlet weak var githubUserBlogLbl: UILabel!
    @IBOutlet weak var githubUserNotesTv: UITextView!
    @IBOutlet weak var githubDetailBoxVw: UIView!
    @IBOutlet weak var saveButtonBtn: UIButton!
    
    // MARK:- Vars
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var validation           = ValidationService()
    var coreDataService      = CoreDataService()
    var githubUserUrl:String = ""
    
    var githubUserDetail:GithubUserDetail?
    var githubUserDetails: [GithubUserDetails] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // delegate notes textarea
        githubUserNotesTv.delegate = self
        
        // insert notes to textarea if exist
        githubUserNotesTv.text = githubUserDetail?.notes
        githubUserNotesTv.layer.borderWidth = 1
        githubUserNotesTv.layer.borderColor = UIColor.black.cgColor
        
        // set border to detailbox
        githubDetailBoxVw.layer.borderWidth = 1
        githubDetailBoxVw.layer.borderColor = UIColor.black.cgColor
        
        // add border to button
        saveButtonBtn.layer.masksToBounds = false
        saveButtonBtn.layer.cornerRadius = 10
        saveButtonBtn.clipsToBounds = true
        
        // load the image from cache using url
        let imageUrl = URL(string: (githubUserDetail?.avatar_url!)!)
        githubUserAvatarImg.loadImage(fromURL: imageUrl!, placeHolderImage: "GithubUserCover", inverted: false)
        
        // DispatchSemaphore to queued 1 request at a time
        let semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: githubUserUrl)!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
            }
            //print(String(data: data, encoding: .utf8)!)
            
            // source: https://www.avanderlee.com/swift/json-parsing-decoding/
            let githubUserDetail: GithubUserDetails = try! JSONDecoder().decode(GithubUserDetails.self, from: data)
            //print(githubUserDetail.login)
            
            DispatchQueue.main.async {
                //self.githubUserAvatarImg = '';
                self.githubUserFollowersLbl.text = "Followers: \(githubUserDetail.followers)"
                self.githubUserFollowingLbl.text = "Followers: \(githubUserDetail.following)"
                self.githubUserNameLbl.text = githubUserDetail.login
                self.githubUserCompanyLbl.text = githubUserDetail.company
                self.githubUserBlogLbl.text = githubUserDetail.blog
            }
            
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func saveNotes(_ sender: Any) {
        do {
            let notes = try validation.validateNote(self.githubUserNotesTv.text);
            coreDataService.updateGithubUser(githubUser: githubUserDetail!, newNote: notes)
            
            self.showAlert(message: "Successfully updated the notes")
            print("Notes saved")
        } catch {
            self.showAlert(message: "\(error)")
            print(error)
        }
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Notes", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: {_ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

// generated from my helper
// https://jsfiddle.net/vandolphreyes29/4ewzb1hq/225/
struct GithubUserDetails: Codable {
    let login: String?
    let id: Int
    let node_id: String?
    let avatar_url: String?
    let gravatar_id: String?
    let url: String?
    let html_url: String?
    let followers_url: String?
    let following_url: String?
    let gists_url: String?
    let starred_url: String?
    let subscriptions_url: String?
    let organizations_url: String?
    let repos_url: String?
    let events_url: String?
    let received_events_url: String?
    let type: String?
    let site_admin: Bool
    let name: String?
    let company: String?
    let blog: String?
    let location: String?
    let email: String?
    let hireable: String?
    let bio: String?
    let twitter_username: String?
    let public_repos: Int
    let public_gists: Int
    let followers: Int
    let following: Int
    let created_at: String?
    let updated_at: String?
}
