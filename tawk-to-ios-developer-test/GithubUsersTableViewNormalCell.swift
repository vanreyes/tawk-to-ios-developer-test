//
//  GithubUsersTableViewCell.swift
//  tawk-to-ios-developer-test
//
//  Created by Vandolph Reyes on 5/27/21.
//

import UIKit

class GithubUsersTableViewNormalCell: UITableViewCell {
    
    @IBOutlet weak var githubUserAvatarImg: LazyImageView!
    @IBOutlet weak var githubUserUsernameLbl: UILabel!
    @IBOutlet weak var notesIconImg: UIImageView!
    @IBOutlet weak var githubUserNodeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
