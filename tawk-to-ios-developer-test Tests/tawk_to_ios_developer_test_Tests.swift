//
//  tawk_to_ios_developer_test_Tests.swift
//  tawk-to-ios-developer-test Tests
//
//  Created by Vandolph Reyes on 5/31/21.
//

import XCTest
@testable import tawk_to_ios_developer_test

class tawk_to_ios_developer_test_Tests: XCTestCase {
    
    var validation: ValidationService!
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func validate_notes() throws {
        XCTAssertNoThrow(try validation.validateNote("wo"))
    }

}
